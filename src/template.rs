use aoc_runner_derive::{aoc, aoc_generator};

type Data = ();

#[aoc_generator(dayXX)]
pub fn input_generator(input: &str) -> Data {
    todo!();
}

#[aoc(dayXX, part1)]
pub fn solve_part1(input: &Data) -> usize {
    todo!();
}

#[aoc(dayXX, part2)]
pub fn solve_part2(input: &Data) -> usize {
    todo!()
}
