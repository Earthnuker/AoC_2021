use aoc_runner_derive::{aoc, aoc_generator};

type Data = Vec<i64>;

#[aoc_generator(day07)]
pub fn input_generator(input: &str) -> Data {
    let mut input: Vec<_> = input.split(',').map(|v| v.parse().unwrap()).collect();
    input.sort_unstable();
    input
}

#[aoc(day07, part1)] // 336040
pub fn solve_part1(input: &Data) -> i64 {
    let start = *input.first().unwrap();
    let end = *input.last().unwrap();
    (start..=end)
        .map(|n| input.iter().map(|v| (v - n).abs()).sum())
        .min()
        .unwrap()
}

#[aoc(day07, part2)] // 94813675
pub fn solve_part2(input: &Data) -> i64 {
    let start = *input.first().unwrap();
    let end = *input.last().unwrap();
    (start..=end)
        .map(|n| {
            input
                .iter()
                .map(|v| {
                    let diff = (v - n).abs();
                    (diff * (diff + 1)) >> 1 // Gauss
                })
                .sum()
        })
        .min()
        .unwrap()
}
