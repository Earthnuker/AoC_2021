#![allow(clippy::clippy::needless_return)]
#![feature(int_abs_diff)]
use aoc_runner_derive::aoc_lib;
pub mod day01;
pub mod day02;
pub mod day03;
pub mod day04;
pub mod day05;
pub mod day06;
pub mod day07;
pub mod day08;
pub mod day09;
pub mod day10;
aoc_lib! { year = 2021 }
