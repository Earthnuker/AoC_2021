use aoc_runner_derive::{aoc, aoc_generator};
#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<usize> {
    input
        .lines()
        .map(|l| l.trim().parse())
        .collect::<Result<Vec<usize>, _>>()
        .unwrap()
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[usize]) -> usize {
    return input.windows(2).filter(|v| v[1] > v[0]).count();
}
#[aoc(day1, part2)]
pub fn solve_part2(input: &[usize]) -> usize {
    input
        .windows(3)
        .map(|w| w.iter().sum())
        .collect::<Vec<usize>>()
        .as_slice()
        .windows(2)
        .filter(|v| v[1] > v[0])
        .count()
}
