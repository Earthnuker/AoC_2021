use aoc_runner_derive::{aoc, aoc_generator};
type Data = Vec<Vec<char>>;

#[aoc_generator(day10)]
pub fn input_generator(input: &str) -> Data {
    input.lines().map(|l| l.chars().collect()).collect()
}

pub fn error_score(line: &Vec<char>) -> (usize,Vec<char>) {
    let mut score=0;
    let mut stack=vec![];
    for c in line {
        match c {
            '(' => {stack.push(')')},
            '[' => {stack.push(']')},
            '{' => {stack.push('}')},
            '<' => {stack.push('>')},
            ')'|']'|'}'|'>' => {
                if let Some(ex) = stack.pop() {
                    if ex!=*c {
                        score+=match c {
                            ')' => 3,
                            ']' => 57,
                            '}' => 1197,
                            '>' => 25137,
                            c => unreachable!("Unexpected character {:?}!",c)
                        };
                        break;
                    }
                }
            },
            c => unreachable!("Unexpected character {:?}!",c)
        }
    }
    (score,stack)
}

#[aoc(day10, part1)]
pub fn solve_part1(input: &Data) -> usize {
    input.iter().map(|v| error_score(v).0).sum()
}

#[aoc(day10, part2)]
pub fn solve_part2(input: &Data) -> usize {
    let mut scores=vec![];
    for line in input {
        let mut line_score=0;
        let (error_score,mut stack) = error_score(line);
        stack.reverse();
        if error_score==0 && !stack.is_empty() {
            for c in stack {
                line_score*=5;
                line_score+=match c {
                    ')' => 1,
                    ']' => 2,
                    '}' => 3,
                    '>' => 4,
                    c => unreachable!("Unexpected character {:?}!",c)
                }
            }
            scores.push(line_score);
        }
    }
    scores.sort_unstable();
    scores[scores.len()/2]
}
