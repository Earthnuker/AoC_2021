use aoc_runner_derive::{aoc, aoc_generator};

type Data = [usize; 9];

#[aoc_generator(day06)]
pub fn input_generator(input: &str) -> [usize; 9] {
    let mut state: [usize; 9] = [0; 9];
    let input = input.trim().split(',').map(|v| v.parse::<usize>().unwrap());
    for v in input {
        state[v] += 1;
    }
    state
}

fn simulate(input: &Data, n: usize) -> usize {
    let mut state = *input;
    for i in 0..n {
        state[(7 + i) % 9] += state[i % 9];
    }
    state.iter().sum()
}

#[aoc(day06, part1)] // 353274
pub fn solve_part1(input: &Data) -> usize {
    simulate(input, 80)
}

#[aoc(day06, part2)] // 1609314870967
pub fn solve_part2(input: &Data) -> usize {
    simulate(input, 256)
}
